<?php
namespace Krasimird\AioraOrderService\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    public $id;

    /**
     * Order constructor.
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->id;
    }

    /**
     * @param mixed $name
     */
    public function setName($id)
    {
        $this->id = $id;
    }




}
