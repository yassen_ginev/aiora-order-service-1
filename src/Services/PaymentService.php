<?php
declare (strict_types=1);

namespace Krasimird\AioraOrderService\Services;

use Krasimird\AioraOrderService\Events\OrderCreated;
use Krasimird\AioraOrderService\Http\Requests\OrderPostRequest;
use Krasimird\AioraOrderService\Http\Responses\OrderResponse;
use Krasimird\AioraOrderService\Models\Order;
use Illuminate\Queue\Queue;
use Illuminate\Queue\SqsQueue;
use Illuminate\Support\Facades\App;
use SchemaQueryParameterProcessor\SchemaQueryParameter;


class PaymentService
{


    private $request;

    /**
     * PaymentService constructor.
     * @param OrderPostRequest $request
     */
    public function __construct(OrderPostRequest $request)
    {
        $this->request = $request;
    }

    public function process() : OrderResponse
    {

        if($this->request->get("process") == 'false'){
            return new OrderResponse( "Payment failed" ,  424   , '');
        }

        $orderId = (string)rand();



        $ord = new Order($orderId);

        OrderCreated::dispatch(new Order($orderId));


        return new OrderResponse("" , 200  , ["orderId" => $orderId]);
    }

}
